cc = gcc
cc_standard = -std=c99
cc_optimization = -Ofast -march=native
cc_warnings = -Werror -Wall -Wextra -Wpedantic -Wshadow -Wconversion -pedantic-errors
cc_warnings_no_werror = -Wall -Wextra -Wpedantic -Wshadow -Wconversion -pedantic-errors
cc_link = 

stts: stts.c
	${cc} ${cc_standard} ${cc_optimization} ${cc_warnings} $^ -o $@ ${cc_link}

stts-nowerror: stts.c
	${cc} ${cc_standard} ${cc_optimization} ${cc_warnings_no_werror} $^ -o stts ${cc_link}

help:
	@echo " "
	@echo "Options: make || make stts-nowerror || make help || make clean"
	@echo " "
	@echo "make ----------------> normal build"
	@echo "make stts-nowerror --> suppress treating warnings as errors"
	@echo "make help -----------> show this help message"
	@echo "make clean ----------> remove produced binary file(s)"

.PHONY: clean
clean:
	rm -rf stts

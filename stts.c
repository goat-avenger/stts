/* stts (simple terminal text streamer)
 * 
 * prints text to standard output character by character
 * with options for rate of output and number of characters
 * per line
 *
 * This is free and unencumbered software released into the public domain.
 * Read UNLICENSE for more information.  */

#if __STDC_VERSION__ < 199901L
  #error "C99 Minimum Required"
#endif

#ifndef _POSIX_C_SOURCE 
  #define _POSIX_C_SOURCE 199309L
//  #define _POSIX_C_SOURCE 200112L
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define OPTPARSE_IMPLEMENTATION
//#define OPTPARSE_API static  // ?
#include "optparse.h"
#include <stdbool.h>

/* String buffer length for word wrap config plus 1 for terminating NULL
 * What might want [TODO], is make this a configurable option that
 * can be passed as an argument at runtime (being that the number of columns
 * in a virtual terminale may vary) 
 * [DONE]  */

//#define BUFF 82
#define NEW_LINE_CHAR 10
#define SPACE_CHAR 32
#define END_OF_STRING '\0'
#define NANO_SECOND_MULTIPLIER 1000000 // = 1 millisecond

/* Any minimum less than 3 means zero output columns, it seems.
 * Errant behavior I don't want to deal with now, seems likely to be 
 * avoided by arbitrarily limmiting min columns to 8, for now.  */
#define MIN_COLUMNS 8
#define MAX_COLUMNS 1024
#define MIN_MS 1
#define MAX_MS 999

void transmit(char stream[], long ms, long buffer);

int main(int argc, char* argv[])
{
  struct optparse_long longopts[] =
  {
    {"help",    'h', OPTPARSE_NONE},
    {"columns", 'c', OPTPARSE_REQUIRED},
    {"speed",   's', OPTPARSE_REQUIRED},
    {0}
  };
  bool help=false;
  long buffer=82;
  long ms=50;

  //char* arg; //?
  int option;
  struct optparse options;
    
  char* cptr;
  char* sptr;
    
  //(void)argc; //?
  optparse_init(&options, argv);
  while ((option = optparse_long(&options, longopts, NULL)) != -1)
  {
    switch (option)
    {
      case 'h':
        help=true;
        break;
      case 'c':
        if ((buffer=strtol(options.optarg, &cptr, 10)) < MIN_COLUMNS || buffer > MAX_COLUMNS)
        {
          //printf("Invalid number of output columns: %ld \n", buffer);
          fprintf(stderr, "Invalid number of output columns: %ld \n", buffer);
	   
          exit (EXIT_FAILURE);
         }
         break;
      case 's':
        if ((ms=strtol(options.optarg, &sptr, 10)) < MIN_MS || ms > MAX_MS)
        {
          //printf("Invaled number of millisecond(s): %ld\n", ms);
          fprintf(stderr, "Invalid number of millisecond(s): %ld \n", ms);

          exit (EXIT_FAILURE);
        }
        break;
      case '?':
        fprintf(stderr, "%s: %s\n", argv[0], options.errmsg);
        exit(EXIT_FAILURE);
    }
  }

  /* [TODO]: Argument checks (Implement this the proper UNIX getopt() way(or optparse.h)?)
   * [DONE (above)]  */
  if (argc == 1)
  {
    printf("\nOops, something isn't right.\nType: \'%s --help\', or \'%s -h\', for more information. \n", argv[0], argv[0]);

    return (0);
  }
  else if (help == true)
  {
    printf("\nUsage: %s [OPTIONS] [FILE]", argv[0]);
    printf("\nExample: %s -s 60 -c 80 text.txt\n", argv[0]);
    
    printf("\nOption \t\tLong Option \t\tMeaning");
    
    printf("\n-c \t\t--columns \t\tnumber of columns for the output");
    printf("\n\t\t\t\t\t(default = 80) (min = 8) (max = 1024)");
    
    printf("\n-h \t\t--help \t\t\tshow this help message");
    
    printf("\n-s \t\t--speed \t\tcharacter output rate in milliseconds");
    printf("\n\t\t\t\t\t(default = 50) (min = 1) (max = 999)\n");

    return (0);
  }
  else
  {
    system("clear");
    transmit(argv[argc-1], ms, buffer);
  }

  return(0);
}

void transmit(char stream[], long ms, long buffer)
{
  FILE *source;
  int buffmain[buffer], /*buffspill[buffer],*/ checked;
  int cntr1=0, cntr2=2, buff_spew_cntr, cntr4=2, check;
  //struct timespec sleep_time = {0 /*secs*/, (ms*NANO_SECOND_MULTIPLIER) /*nanoseconds*/};
  struct timespec sleep_time;
    sleep_time.tv_sec = 0;
    sleep_time.tv_nsec = ms*NANO_SECOND_MULTIPLIER;

  if ((source=fopen(stream, "r"))==NULL)
  {
    fprintf(stderr,"\n\nError opening file\n\n");
    exit(EXIT_FAILURE);
  }

  while (1)
  {
    // Check for new line character and flush/spew if found.
    while (cntr1<(buffer-cntr2) && (buffmain[cntr1]=getc(source))!=EOF)
    {
      if (buffmain[cntr1]==NEW_LINE_CHAR)
      {
        for (buff_spew_cntr=0; buffmain[buff_spew_cntr]!=NEW_LINE_CHAR; buff_spew_cntr++)
        {
          printf("%c", buffmain[buff_spew_cntr]);
          fflush(stdout);
          nanosleep(&sleep_time, &sleep_time);
        }
        /* printf("\n"); 
         * [PROBLEM]: Is there a way to print this from the file stream?
         * [YES, FIXED, DONE]  */
        printf("%c", buffmain[buff_spew_cntr]);
        fflush(stdout);
        nanosleep(&sleep_time, &sleep_time);
  
        cntr1=0;
  
        continue;
      }
      else
        cntr1++;
    }
 
    // If EOF, exit/return (0)
    if (buffmain[cntr1]==EOF)
    /* [PROBLEM]: Below --> Trying to fix last bits of a text file not printing.
     * [FIXED, DONE] */
    {
      for (buff_spew_cntr=0; buffmain[buff_spew_cntr]!=EOF; buff_spew_cntr++)
      {
        printf("%c", buffmain[buff_spew_cntr]);
        fflush(stdout);
        nanosleep(&sleep_time, &sleep_time);
      }
      break;
    }
 
    // Terminate string
    buffmain[cntr1]=END_OF_STRING;
    // Rewind to previous character
    cntr1--;
    // Keep a record of our buffer length.
    check=cntr1;
 
    //Word wrapper: checks for a space or if none is found stops wrapping at end of buffer
    while (buffmain[cntr1]!=SPACE_CHAR && cntr1>0)
    {
      cntr1--;
      cntr4++; // record of number of characters till rewind hits a space
    }
    cntr2=cntr4;

    // When no space is found... 
    if (cntr1==0)
    {
        buffmain[buffer-2]=NEW_LINE_CHAR;
        buffmain[buffer-1]=END_OF_STRING;
    }
        
    if (buffmain[cntr1]==SPACE_CHAR)
      buffmain[cntr1]=NEW_LINE_CHAR;
        
    for (buff_spew_cntr=0; buffmain[buff_spew_cntr]!=END_OF_STRING; buff_spew_cntr++)
    {
      printf("%c", buffmain[buff_spew_cntr]);
      fflush(stdout);
      nanosleep(&sleep_time, &sleep_time);
    }
    /* START: Check to see if last char in string is a space 
     * and if it is check to make sure the next character is not also a space.
     * If it is also a space, ignore it, it falls after the wrap.  */
    if (cntr1==check)
    {
      checked=getc(source);
      if (checked==SPACE_CHAR)
      {
        cntr1=0;
        continue;
      }
      else
      {
        buffmain[0]=checked;
        cntr1=1;
        continue;
      }
    }
    cntr1=0;
    cntr4=2;
    /*END*/
  }
  /* Add a couple of new line characters to prevent last line being
   * overwritten by the shell prompt..  
   *
   * [PROBLEM]: This doesn't help, something is broken above
   * [FIXED, DONE, SEE ABOVE, SEE BELOW FOR WHY]
   * The for loop responsible for checking for return characters was 
   * conditioned on hitting EOF.  However, it wasn't purging the
   * the buffer after hitting EOF.  So, if no new line character 
   * was at the end of the document, whatever got hoovered up 
   * into the buffer, at the end, wasn't purged/printed 
   * the fix was to print out the remains of the buffer 
   * if EOF was hit.  The new lines at the end do help as well though.
   * The new lines prevent the shell prompt from overwriting streamed 
   * text.  */
  printf("\n\n");
  fclose(source);
}


## stts - simple terminal text streamer

This is a simple C program to print text files, character by character, to stdout inside a terminal or virtual terminal.  There are options to control the delay between each character printed, as well as how many characters to print before a newline is started (-s/--speed and -c/--columns, respectively).

To compile, running, make, on a GNU system or other UNIX-like system should work.  

This is free and unencumbered software released into the public domain.
Read UNLICENSE for more information.
